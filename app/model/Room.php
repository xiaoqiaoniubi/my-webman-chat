<?php

namespace app\model;

use support\Model;

/**
 * @property integer $id (主键)
 * @property string $room_name 房间名称
 * @property integer $master_id 房主id
 * @property integer $created_at 创建时间
 * @property integer $updated_at 最后重启时间
 */
class Room extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'room';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    protected $dateFormat = 'U';

    /**
     * @param int $uid
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     * @author xqiaochina@163.com
     * 根据uid获取用户房间信息
     */
    public function getUserRoom(int $uid)
    {
        return $this -> where('master_id',$uid) -> first();
    }

    /**
     * @param $uid
     * @param $roomName
     * @return bool|int
     * @author xqiaochina@163.com
     * 创建房间
     */
    public function createRoom($uid,$roomName)
    {
        $info = $this -> where('master_id',$uid) -> first();
        if (!empty($info)) { // 用户已存在房间
            return false;
        }
        $this -> room_name = $roomName;
        $this -> master_id = $uid;
        $result = $this -> save();
        if (!$result) {
            return false;
        }
        return $this -> id;
    }

    /**
     * @param array $ids
     * @return \Illuminate\Database\Eloquent\Collection[]|\Illuminate\Support\Collection
     * @author xqiaochina@163.com
     * 根据id列表获取房间列表
     */
    public function getRoomListByIds(array $ids = [])
    {
        if (empty($ids)) {
            return $this -> get();
        } else {
            return $this -> whereIn('id',$ids) -> get();
        }
    }

}
