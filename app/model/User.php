<?php

namespace app\model;

use support\Model;

/**
 * @property integer $id (主键)
 * @property string $nickname 用户昵称
 * @property string $password 用户密码
 * @property string $token 登录令牌
 * @property integer $created_at 创建时间
 * @property integer $updated_at 最后修改时间
 */
class User extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    protected $dateFormat = 'U';


    // 密码加盐
    protected const SALT = 'chat';

    /**
     * @param string $password
     * @return string
     * @author xqiaochina@163.com
     * 创建用户密码
     */
    protected function createPassword(string $password) : string
    {
        // 密码这里随便加密一下
        return md5($password . self::SALT);
    }

    /**
     * @return string
     * @author xqiaochina@163.com
     * 创建Token
     */
    protected function getToken() : string
    {
        return md5(uniqid()) . md5(uniqid());
    }

    /**
     * @param array $userInfo
     * @return bool|string
     * @author xqiaochina@163.com
     * 用户登录
     */
    public function doLogin(array $userInfo)
    {
        $uInfo = $this -> where('nickname',$userInfo['nickname']) -> first();
        if (empty($uInfo)) {
            // 不存在 即 注册
            $this -> nickname = $userInfo['nickname'];
            $token = $this -> getToken();
            $this -> token = $token;
            $this -> password = $this -> createPassword($userInfo['password']);
            $result = $this -> save();
            return !$result == true ? false : $token;
        } else { // 验证密码
            if ($this -> createPassword($userInfo['password']) == $uInfo -> password) {
                $token = $this -> getToken();
                $uInfo -> token = $token;
                $result = $uInfo -> save();
                return !$result == true ? false : $token;
            }
            return false;
        }
    }

    /**
     * @param string $token
     * @return bool
     * @author xqiaochina@163.com
     * 验证token
     */
    public function checkToken(string $token) : bool
    {
        $uInfo = $this -> where('token',$token) -> first();
        return !!$uInfo;
    }

    /**
     * @param string $token
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     * @author xqiaochina@163.com
     * 根据用户token查询用户信息
     */
    public function getUserInfoByToken(string $token)
    {
        return $this -> where('token',$token) -> first();
    }

    /**
     * @param string $token
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     * @author xqiaochina@163.com
     * 根据用户id查询用户信息
     */
    public function getUserInfoByUid($id)
    {
        return $this -> where('id',$id) -> first();
    }

}
