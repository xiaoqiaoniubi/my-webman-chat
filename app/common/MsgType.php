<?php

namespace app\common;

class MsgType
{
    const PING = 'ping'; // PING
    const LOGIN = 'login'; // 登录
    const ERROR = 'error'; // 发生错误
    const LOGIN_ERROR = 'login_error'; // 发生错误
    const JOIN = 'join'; // 加入房间
    const SEND_MSG = 'send_msg'; // 发送信息
}
