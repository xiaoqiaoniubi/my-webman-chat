<?php

use GatewayWorker\Lib\Gateway;

/**
 * Here is your custom functions.
 * @param $clientId
 * @param array $msg
 */

/**
 * @param $clientId
 * @param string $msg
 * @param array $data
 * @author xqiaochina@163.com
 * 向个人发送消息
 */
function send($clientId,string $msg = '请求成功',array $data = []) : void
{
    Gateway::sendToClient($clientId, json_encode([
        'msg' => $msg,
        'data' => $data,
    ],JSON_UNESCAPED_UNICODE));
}

/**
 * @param $roomId
 * @param string $msg
 * @param array $data
 * @author xqiaochina@163.com
 * 向群组发送消息
 */
function send_to_group($roomId,string $msg = '请求成功',array $data = []) : void
{
    Gateway::sendToGroup($roomId, json_encode([
        'msg' => $msg,
        'data' => $data,
    ],JSON_UNESCAPED_UNICODE));
}