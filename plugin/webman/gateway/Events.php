<?php

namespace plugin\webman\gateway;

use app\common\MsgType;
use app\model\Room;
use app\model\User;
use GatewayWorker\Lib\Gateway;

class Events
{

    public static function onWorkerStart($worker)
    {

    }

    public static function onConnect($client_id)
    {

    }

    public static function onWebSocketConnect($client_id, $data)
    {
        // 拿到连接时携带的token 查询uid
        $getData = $data['get'];
        $userModel = new User();
        $userInfo = $userModel -> getUserInfoByToken($getData['token']);
        if (empty($userInfo)) { // token过期
            send($client_id,'账号信息错误，请重新登录',[
                'type' => MsgType::LOGIN_ERROR,
            ]);
            sleep(1); // 延迟一秒断开链接
            return Gateway::closeClient($client_id);
        }
        $uid = $userInfo -> id;
        // 重复上线验证
        if (count(Gateway::getClientIdByUid($uid)) > 0) {
            send($client_id,'当前账号已在线,请换个账号',[
                'type' => MsgType::ERROR,
            ]);
            sleep(1); // 延迟一秒断开链接
            return Gateway::closeClient($client_id);
        }
        // uid 绑定 client_id
        Gateway::bindUid($client_id,$uid);
        // 用户是否创建过房间，是否显示创建房间 or 我的房间 按钮
        $roomModel = new Room();
        $roomInfo = $roomModel -> getUserRoom($uid);
        $roomList = $roomModel -> getRoomListByIds();
        send($client_id,'连接成功',[
            'client_id' => $client_id,
            'type' => MsgType::LOGIN,
            'room_id' => empty($roomInfo) ? 0 : $roomInfo -> id,
            'room_list' => $roomList,
        ]);
    }

    public static function onMessage($client_id, $message)
    {
        $message = json_decode($message,true);
        $data = $message['data'] ?? [];
        switch ($message['type']) {
            case MsgType::JOIN: // 加入房间
                $userModel = new User();
                $userInfo = $userModel -> getUserInfoByToken($data['token']);
                // 加入房间
                Gateway::joinGroup($client_id,$data['room_id']);
                // 获取房间列表
                $roomUserList = Gateway::getUidListByGroup($data['room_id']);
                $finalUserList = [];
                foreach ($roomUserList as $key => $val) {
                    // 查询用户昵称
                    $userInfo = $userModel -> getUserInfoByUid($val);
                    $finalUserList[] = ['user_id' => $val,'nickname' => empty($userInfo) ? '未知用户' : $userInfo -> nickname];
                }
                // 向房间广播
                send_to_group($data['room_id'],'请求成功',[
                    'type' => MsgType::JOIN,
                    'nickname' => $userInfo -> nickname,
                    'online_num' => count($finalUserList),
                    'online_list' => $finalUserList,
                ]);
                break;
            case MsgType::SEND_MSG:
                $userModel = new User();
                $userInfo = $userModel -> getUserInfoByToken($data['token']);
                // 广播到房间
                send_to_group($data['room_id'],'请求成功',[
                    'type' => MsgType::SEND_MSG,
                    'send_user_nickname' => $userInfo -> nickname,
                    'send_user_id' => $userInfo -> id,
                    'send_content' => $data['content'],
                ]);
                break;
            default:
                send($client_id,'请求成功',['client_id' => $client_id]);
                break;
        }
    }

    public static function onClose($client_id)
    {

    }

}
