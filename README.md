# 初始化项目
1. 先下载下项目，将根目录下的webman-chat.sql 导入至数据库，数据库名称为：webman-chat。可在数据库配置文件中修改
2. 执行 composer install

# 运行
1. windows环境：直接双击根目录下的windows.bat的文件，并在网站中访问localhost:8787
2. linux环境：防火墙等开放7272端口（websocket），在根目录上执行 php start.php start